package datastorage;

import model.Nurse;
import model.User;
import utils.DateConverter;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Implements the Interface <code>DAOImp</code>. Overrides methods to generate specific User-SQL-queries.
 */

public class UsersDAO extends DAOimp<User>{

    /**
     * constructs Onbject. Calls the Constructor from <code>DAOImp</code> to store the connection.
     * @param conn
     */

    public UsersDAO(Connection conn) {
        super(conn);
    }

    /**
     * generates a <code>INSERT INTO</code>-Statement for a given patient
     * @param user for which a specific INSERT INTO is to be created
     * @return <code>String</code> with the generated SQL.
     */
    @Override
    protected String getCreateStatementString(User user){
        return String.format("INSERT INTO users (username, password) VALUES ('%s', '%s')",
                user.getUsername(), user.getPassword());
    }

    /**
     * generates a <code>select</code>-Statement for a given key
     return String.format("SELECT * FROM users WHERE id = %d", key);
     * @param key for which a specific SELECT is to be created
     * @return <code>String</code> with the generated SQL.
     */
    @Override
    protected String getReadByIDStatementString(int key) {
        return String.format("SELECT * FROM users WHERE id = %d", key);
    }

    /**
     * maps a <code>ResultSet</code> to a <code>Patient</code>
     * @param result ResultSet with a single row. Columns will be mapped to a User-object.
     * @return User with the data from the resultSet.
     */
    @Override
    protected User getInstanceFromResultSet(ResultSet result) throws SQLException {
        User u = null;
        u = new User(result.getInt(1), result.getString(2),
                result.getString(3));
        return u;
    }

    /**
     * generates a <code>SELECT</code>-Statement for all Users.
     * @return <code>String</code> with the generated SQL.
     */
    @Override
    protected String getReadAllStatementString() {
        return "SELECT * FROM users";
    }

    /**
     * maps a <code>ResultSet</code> to a <code>User-List</code>
     * @param result ResultSet with a multiple rows. Data will be mapped to User-object.
     * @return ArrayList with Users from the resultSet.
     */
    @Override
    protected ArrayList<User> getListFromResultSet(ResultSet result) throws SQLException {
        ArrayList<User> list = new ArrayList<User>();
        User u = null;
        while (result.next()) {
            u = new User(result.getInt(1), result.getString(2),
                    result.getString(3));
            list.add(u);
        }
        return list;
    }

    /**
     * generates a <code>UPDATE</code>-Statement for a given User
     * @param user for which a specific update is to be created
     * @return <code>String</code> with the generated SQL.
     */
    @Override
    protected String getUpdateStatementString(User user) {
        return String.format("UPDATE user SET username = '%s', password = '%s' WHERE id = %d",
                user.getUsername(), user.getPassword());
    }

    /**
     * generates a <code>delete</code>-Statement for a given key
     * @param key for which a specific DELETE is to be created
     * @return <code>String</code> with the generated SQL.
     */
    @Override
    protected String getDeleteStatementString(int key) {
        return String.format("Delete FROM users WHERE id=%d", key);
    }
}
