package model;

import utils.DateConverter;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Nurses work in this institution and take care of the Patients.
 */
public class Nurse extends Person{

    private long nid;
    private LocalDate dateOfBirth;
    private String telephone;
    private List<Nurse> allNurses = new ArrayList<>();

    /**
     * constructs a nurse from the given parameters
     * @param firstName
     * @param surname
     * @param dateOfBirth
     * @param telephone
     */
    public Nurse(String firstName, String surname, LocalDate dateOfBirth, String telephone) {
        super(firstName, surname);
        this.dateOfBirth = dateOfBirth;
        this.telephone = telephone;
    }

    /**
     * constructs a nurse from the given parameters
     * @param nid
     * @param firstName
     * @param surname
     * @param dateOfBirth
     * @param telephone
     */
    public Nurse(long nid, String firstName, String surname, LocalDate dateOfBirth, String telephone) {
        super(firstName, surname);
        this.nid = nid;
        this.dateOfBirth = dateOfBirth;
        this.telephone = telephone;
    }

    /**
     *
     * @return id of the nurse
     */
    public long getNid(){

        return nid;

    }

    /**
     *
     * @return date of birth in form of a String
     */
    public String getDateOfBirth(){

        return dateOfBirth.toString();

    }

    /**
     * convert given param to a localDate and store as new <code>dateOfBirth</code>
     * @param dateOfBirth as string in the following format: YYYY-MM-DD
     */
    public void setDateOfBirth(String dateOfBirth) {
        LocalDate birthday = DateConverter.convertStringToLocalDate(dateOfBirth);
        this.dateOfBirth = birthday;
    }

    /**
     *
     * @return telephone number as a string
     */
    public String getTelephone(){

        return telephone;

    }

    /**
     *
     * @param telephone new telephone number
     */
    public void setTelephone(String telephone){

        this.telephone = telephone;

    }

    /**
     *
     * @return all data in form of a String
     */
    public String toString(){

        return "Nurse" + "\nID:\t" + nid + "\nFirstname:\t" + this.getFirstName() + "\nSurname:\t" +
                this.getSurname() + "\nBirthday:\t" + dateOfBirth + "\nTel.:\t" + telephone;
    }

}
