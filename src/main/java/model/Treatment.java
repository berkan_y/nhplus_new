package model;

import utils.DateConverter;
import java.time.LocalDate;
import java.time.LocalTime;

/**
 * all patients receive treatments
 */
public class Treatment {
    private long tid;
    private long pid;
    private long nid;
    private LocalDate date;
    private LocalTime begin;
    private LocalTime end;
    private String description;
    private String remarks;
    private boolean blocked;

    /**
     * constructs a treatment with the given params
     * @param pid
     * @param nid
     * @param date
     * @param begin
     * @param end
     * @param description
     * @param remarks
     * @param blocked
     */
    public Treatment(long pid, long nid, LocalDate date, LocalTime begin,
                     LocalTime end, String description, String remarks, boolean blocked) {
        this.pid = pid;
        this.nid = nid;
        this.date = date;
        this.begin = begin;
        this.end = end;
        this.description = description;
        this.remarks = remarks;
        this.blocked = blocked;
    }

    /**
     * constructs a treatment with the given params
     * @param tid
     * @param pid
     * @param nid
     * @param date
     * @param begin
     * @param end
     * @param description
     * @param remarks
     * @param blocked
     */
    public Treatment(long tid, long pid, long nid, LocalDate date, LocalTime begin,
                     LocalTime end, String description, String remarks, boolean blocked) {
        this.tid = tid;
        this.pid = pid;
        this.nid = nid;
        this.date = date;
        this.begin = begin;
        this.end = end;
        this.description = description;
        this.remarks = remarks;
        this.blocked = blocked;
    }

    /**
     *
     * @return treatment id
     */
    public long getTid() {
        return tid;
    }

    /**
     *
     * @return patient id
     */
    public long getPid() {
        return this.pid;
    }

    /**
     *
     * @return nurse id
     */
    public long getNid() { return this.nid; }

    /**
     *
     * @return date of treatment
     */
    public String getDate() {
        return date.toString();
    }

    /**
     *
     * @return beginning time of treatment
     */
    public String getBegin() {
        return begin.toString();
    }

    /**
     *
     * @return ending time of treatment
     */
    public String getEnd() {
        return end.toString();
    }

    /**
     *
     * @return if the data is blocked or not
     */
    public boolean getBlocked() { return this.blocked; }

    /**
     *  sets the data into a blocked state
     * @param blocked
     */
    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }

    /**
     * sets a new birthday
     * @param s_date
     */
    public void setDate(String s_date) {
        LocalDate date = DateConverter.convertStringToLocalDate(s_date);
        this.date = date;
    }

    /**
     * new beginnig time
     * @param begin
     */
    public void setBegin(String begin) {
        LocalTime time = DateConverter.convertStringToLocalTime(begin);
        this.begin = time;
    }

    /**
     * new ending time
     * @param end
     */
    public void setEnd(String end) {
        LocalTime time = DateConverter.convertStringToLocalTime(end);
        this.end = time;
    }

    /**
     *
     * @return description of the treatment
     */
    public String getDescription() {
        return description;
    }

    /**
     * new description of a treatment
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return remarks
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * new remarks
     * @param remarks
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     *
     * @return all the data in form of a string
     */
    public String toString() {
        return "\nBehandlung" + "\nTID: " + this.tid +
                "\nPID: " + this.pid +
                "\nNID: " + this.nid +
                "\nDate: " + this.date +
                "\nBegin: " + this.begin +
                "\nEnd: " + this.end +
                "\nDescription: " + this.description +
                "\nRemarks: " + this.remarks +
                "\nBlocked: " + this.blocked +
                "\n";
    }
}